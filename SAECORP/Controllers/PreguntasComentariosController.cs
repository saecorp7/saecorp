﻿using SAECORP.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAECORP.Controllers
{
    public class PreguntasComentariosController : Controller
    {
        string CS = ConfigurationManager.ConnectionStrings["SAECORP"].ConnectionString;
        string codsms = "";
        string sms = "";
        string titlesms = "";
        object[] answer;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddEditUser(Comentario comentario)
        {
            if (ModelState.IsValid)
            {
                using (SqlConnection con = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@Nombre", comentario.Nombre);
                    cmd.Parameters.AddWithValue("@Email", comentario.Email);
                    cmd.Parameters.AddWithValue("@Tipo", comentario.Tipo);
                    cmd.Parameters.AddWithValue("@Mensaje", comentario.Mensaje);
                    cmd.CommandText = @"EXEC SPRegistrarComentarios @Nombre, @Email, @Tipo, @Mensaje";
                    try
                    {
                        con.Open();
                        using(SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                codsms = reader.GetString(0);
                                sms = reader.GetString(1);
                                titlesms = reader.GetString(2);
                            }
                        }
                        con.Close();
                    }catch (Exception e)
                    {

                    }
                }
                //if (model.Id.HasValue)
                //{
                //    StaticData.Update(model);
                //}
                //else
                //{
                //    StaticData.Save(model);
                //}
            }
            //string status = codsms/*model.Id.HasValue ? "updated" : "saved"*/;
            ////string message = $"User has been { status } successfully!";
            //string message = $"{codsms}" ;
            answer = new object[] { codsms, sms, titlesms };
            return Json(answer, JsonRequestBehavior.AllowGet);
        }
    }
}