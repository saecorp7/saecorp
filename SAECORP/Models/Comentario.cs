﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAECORP.Models
{
    public class Comentario
    {
        public int IdComentario { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Tipo { get; set; }

        public string Mensaje { get; set; }

    }
}