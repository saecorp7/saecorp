﻿namespace SAECORP.Models
{
    public class LoadMenu
    {
        public int IdMenu { get; set; }
        public int TipoMenu { get; set; }
        public int Grupo { get; set; }
        public int Orden { get; set; }
        public string Descripcion { get; set; }
        public string DirUrl { get; set; }
        public string Icono { get; set; }
    }
}