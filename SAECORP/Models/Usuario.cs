﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAECORP.Models
{
    public class Usuario
    {
        public int MyProperty { get; set; }
        public int IdUsuario { get; set; }
        public int IdPersona { get; set; }
        public string NombreUsuario { get; set; }
        public string Contrasena { get; set; }
        public string Email { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaBaja { get; set; }
    }
}