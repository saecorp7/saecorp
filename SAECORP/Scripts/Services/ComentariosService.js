﻿app.service("ComentariosService", function ($http) {
    var $this = this;
    $this.AddEdit = function (comentario) {
        var request = $http({
            method: 'POST',
            url: 'http://localhost:51245/PreguntasComentarios/AddEditUser',
            data: JSON.stringify(comentario),
            dataType: "json"
        });
        return alert(request);
    }

    $this.Delete = function (id) {
        var request = $http({
            method: 'POST',
            url: "http://localhost:51245/PreguntasComentarios/" + 'DeleteUser',
            data: "{ id:" + id + " }",
            dataType: "json"
        });
        return request;
    }

    $this.GetAll = function () {
        var request = $http({
            method: 'GET',
            url: "http://localhost:51245/PreguntasComentarios/" + 'GetAllUsers',
        });
        return request;
    }

    $this.GetUser = function (id) {
        var request = $http({
            method: 'GET',
            url: "http://localhost:51245/PreguntasComentarios/" + 'GetUser/' + id,
        });
        return request;
    }
});