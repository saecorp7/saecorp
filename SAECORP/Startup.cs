﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SAECORP.Startup))]
namespace SAECORP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
